<?php

/**
 * Created by PhpStorm.
 * User: tysonwarner
 * Date: 4/2/16
 * Time: 2:08 PM
 */
class Contact
{
    private $id;
    private $firstName;
    private $lastName;
    private $street;
    private $state;
    private $city;
    private $zip;
    private $dob;

    public function __construct($id, $firstName, $lastName, $street, $state, $city, $zip, $dob) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->street = $street;
        $this->state = $state;
        $this->city = $city;
        $this->zip = $zip;
        $this->dob = $dob;
    }

    public function __toString() {
        return "$this->id|$this->firstName|$this->lastName|$this->street|$this->state|$this->city|$this->zip|$this->dob|";
    }
}