<?php
/**
 * Created by PhpStorm.
 * User: tysonwarner
 * Date: 4/2/16
 * Time: 9:38 PM
 */
include "config.php";

$attribute = $_GET["attribute"];

echo "<table class='table table-bordered table-striped'>";
echo "<tr><td class='columnHeader'>First Name</td><td class='columnHeader'>Last Name</td>
          <td class='columnHeader'>Date of Birth</td><td class='columnHeader'>Street Address</td>
          <td class='columnHeader'>State</td><td class='columnHeader'>City</td>
          <td class='columnHeader'>Zip Code</td><td class='columnHeader'>Edit</td>
          <td class='columnHeader'>Delete</td><td class='columnHeader'>Get Directions</td></tr>";
$contactArray = array();
$conn = getConn();
mysqli_select_db($conn, "availity");
$stmt = "SELECT * FROM CONTACT ORDER BY " . $attribute;
$result = $conn->query($stmt);
if ($result->num_rows > 0) {
    for ($i = 0; $row = $result->fetch_assoc(); $i++) {
        $id = $row["ID"];
        $first = $row["FIRSTNAME"];
        $last = $row["LASTNAME"];
        $dob = $row["DOB"];
        $street = $row["STREET"];
        $state = $row["STATE"];
        $city = $row["CITY"];
        $zip = $row["ZIPCODE"];
        $editParams = "id=" . urlencode($id) . "&first=" . urlencode($first) . "&last=" . urlencode($last) . "&dob=" . urlencode($dob) . "&street=" . urlencode($street) . "&state=" . urlencode($state) . "&city=" . urlencode($city) . "&zip=" . urlencode($zip);
        $destination = urlencode("$street $city, $state $zip");
        echo "<tr><td>$first</td><td>$last</td><td>$dob</td><td>$street</td><td>$state</td>
              <td>$city</td><td>$zip</td>
              <td><a href='editForm.php?$editParams' class='btn btn-primary'>Edit</a></td>
              <td><a href='contactFunctions.php?getAction=deleteContact&id=$id' class='btn btn-danger'>Delete</a></td>
              <td><a href='directions.php?destination=$destination' class='btn btn-primary'>Get Directions</a></td></tr>";
    }
}
echo "</table>";