# README

> A simple contact management application

## About

This is a contact management application that allows a user to create, edit, and delete records containing first name, last name, date of birth, street address, city, state, and zip code. It also uses the Google Maps Directions API (https://developers.google.com/maps/documentation/directions) to allow users to find directions to a contact's location.

## Developer stack

* PHP
* MySQL
* jQuery
* CSS

## Setup

The schema (absent data) is in this repo and is titled `availity.sql`. The schema is this:

| Field     | Type        | Null | Key | Default | Extra          |
| --------- | ----------- | ---- | --- | ------- | -------------- |
| ID        | int(4)      | NO   | PRI | NULL    | auto_increment |
| FIRSTNAME | varchar(30) | NO   |     | NULL    |                |
| LASTNAME  | varchar(30) | NO   |     | NULL    |                |
| DOB       | date        | NO   |     | NULL    |                |
| STREET    | varchar(75) | NO   |     | NULL    |                |
| STATE     | varchar(20) | NO   |     | NULL    |                |
| CITY      | varchar(30) | NO   |     | NULL    |                |
| ZIPCODE   | varchar(10) | NO   |     | NULL    |                |

In order to access the database in your code, edit `config.php` and change `$username` to `your_username` and `$password` to `your_password`.

## Contact

If you have any questions, contact Tyson Warner at tysonwarner22@gmail.com