<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
    <title>Directions</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/availity-internship.css">
</head>
<div class="container">
    <div class="jumbotron">
        <h1>Directions</h1>
        <?php
        /**
         * Created by PhpStorm.
         * User: tysonwarner
         * Date: 4/2/16
         * Time: 2:07 PM
         */
        $destination = urlencode($_GET["destination"]);

        $origin = urlencode("10752 Deerwood Park Blvd S. Ste 110, Jacksonville, FL 32256");

        $directionsQuery = "https://maps.googleapis.com/maps/api/directions/json?origin=$origin&destination=$destination";
        $directions = file_get_contents($directionsQuery);
        $directionsArray = json_decode($directions, true);

        $jsonIterator = new RecursiveIteratorIterator(
            new RecursiveArrayIterator(json_decode($directions, TRUE)),
            RecursiveIteratorIterator::SELF_FIRST);


        $instructionsArray = array();
        $i = 0;
        foreach ($jsonIterator as $key => $val) {
            if(is_array($val)) {
                //echo "$key:<br>";
            } else {
                if ($key === "html_instructions") {
                    $instructionsArray[$i] = $val;
                    $i++;
                }
            }
        }
        echo "<h3>Origin: " . urldecode($origin) . "</h3>";
        echo "<h3>Destination: " . urldecode($destination) . "</h3>";
        if (count($instructionsArray) > 0) {
            echo "<table class='table table-bordered table-striped'>";
            for ($j = 0; $j < count($instructionsArray); $j++) {
                $instruction = $instructionsArray[$j];
                echo "<tr><td class='step'>Step " . ($j + 1) . ":</td><td class='instruction'>" . $instruction . "</td></tr>";
            }
            echo "</table>";
        } else {
            echo "Whoops. Couldn't find that address. Try reformatting it <br>";
        }
        ?>

        <a href="index.php" class="btn btn-primary">Back to my contacts</a>
    </div>
</div>
</html>
