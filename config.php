<?php
/**
 * Created by PhpStorm.
 * User: tysonwarner
 * Date: 4/4/16
 * Time: 3:30 PM
 */

function getConn() {
    $username = "your_username_here";
    $database = "availity";
    $server = "localhost";
    $password = "your_password_here";

    static $conn = null;
    if (is_null($conn)) {
        $conn = $conn = mysqli_connect($server, $username, $password);
    }
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
    return $conn;
}