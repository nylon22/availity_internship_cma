<?php
/**
 * Created by PhpStorm.
 * User: tysonwarner
 * Date: 4/2/16
 * Time: 2:34 PM
 */

function __autoload($class_name) {
    require_once($class_name . ".php");
}
__autoload("Contact");
include "config.php";

if (isset($_POST['postAction'])) {
    switch ($_POST['postAction']) {
        case "addContact":
            $first = $_POST["first"];
            $last = $_POST["last"];
            $dob = $_POST["dob"];
            $street = $_POST["street"];
            $state = $_POST["state"];
            $city = $_POST["city"];
            $zip = $_POST["zip"];
            insertRecord($first, $last, $dob, $street, $state, $city, $zip);
            header("Location: index.php");
            break;
        case "editContact":
            $id = (int) $_POST["id"];
            $first = $_POST["first"];
            $last = $_POST["last"];
            $dob = $_POST["dob"];
            $street = $_POST["street"];
            $state = $_POST["state"];
            $city = $_POST["city"];
            $zip = $_POST["zip"];
            updateRecord($id, $first, $last, $dob, $street, $state, $city, $zip);
            header("Location: index.php");
            break;
    }
} elseif (isset($_GET['getAction'])) {
    switch ($_GET['getAction']) {
        case "deleteContact":
            $id = (int) $_GET["id"];
            deleteRecord($id);
            header("Location: index.php");
            break;
    }
} else {
    header("Location: index.php");
}


function insertRecord($first, $last, $dob, $street, $state, $city, $zip) {
    $conn = getConn();
    mysqli_select_db($conn, "availity") or die("Unable to connect");
    $stmt = $conn->prepare("INSERT INTO CONTACT (FIRSTNAME, LASTNAME, DOB, STREET, STATE, CITY, ZIPCODE) VALUES(?, ?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("sssssss", $first, $last, $dob, $street, $state, $city, $zip);
    $stmt->execute();
    $stmt->close();
    $conn->close();
}

function updateRecord($id, $first, $last, $dob, $street, $state, $city, $zip) {
    $conn = getConn();
    mysqli_select_db($conn, "availity") or die("Unable to connect");
    $stmt = $conn->prepare("UPDATE CONTACT SET FIRSTNAME=?, LASTNAME=?, DOB=?, STREET=?, STATE=?, CITY=?, ZIPCODE=? WHERE ID=?");
    $stmt->bind_param("sssssssi", $first, $last, $dob, $street, $state, $city, $zip, $id);
    $stmt->execute();
    $stmt->close();
    $conn->close();
}

function deleteRecord($id) {
    $conn = getConn();
    mysqli_select_db($conn, "availity") or die("Unable to connect");
    $stmt = $conn->prepare("DELETE FROM CONTACT WHERE ID=?");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->close();
    $conn->close();
}
