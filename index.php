<?php
/**
 * Created by PhpStorm.
 * User: tysonwarner
 * Date: 3/29/16
 * Time: 12:59 PM
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
    <title>My Contacts</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/availity-internship.css">
</head>
<body onload="orderContactsByLastName();">
<div class="container">
    <div class="jumbotron">
        <h1>My Contacts</h1>
        <br><br>
        <div class="row" style="padding: 15px;">
            <div class="form-group">
                <div class="col-lg-6">
                    <label for="filter">Order Contacts</label>
                    <select class="form-control" id="filter">
                        <option selected value="LASTNAME">Order By Last Name</option>
                        <option value="FIRSTNAME">Order By First Name</option>
                        <option value="ZIPCODE">Group By Zip Code</option>
                        <option value="STATE">Order By State</option>
                        <option value="CITY">Order By City</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="btn-group btn-group-justified" role="group">
                    <div class='btn-group' role='group'>
                        <a class="btn btn-lg btn-success" href="addForm.php">Add Contact</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="contacts"></div>
</body>
</div>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="js/orderContacts.js"></script>
</html>




