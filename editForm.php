<?php
/**
 * Created by PhpStorm.
 * User: tysonwarner
 * Date: 4/2/16
 * Time: 4:58 PM
 */
$id = urldecode($_GET["id"]);
$first = urldecode($_GET["first"]);
$last = urldecode($_GET["last"]);
$dob = urldecode($_GET["dob"]);
$street = urldecode($_GET["street"]);
$state = urldecode($_GET["state"]);
$city = urldecode($_GET["city"]);
$zip = urldecode($_GET["zip"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
        <title>Edit Contact Form</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/availity-internship.css">
</head>
<div class="container">
        <div class="jumbotron">
                <h1>Edit Contact Form</h1>
                <form name="editForm" id="editForm" action="contactFunctions.php" method="post">
                        <input type="hidden" name="postAction" value="editContact">
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <div class="form-group">
                                <table>
                                        <tr><td class="addEditLabel">First Name:</td><td><input type="text" class="form-control" name="first" id="first" value="<?php echo $first;?>"></td><td><p class="error" id="firstError"></p></td></tr>
                                        <tr><td class="addEditLabel">Last Name:</td><td><input type="text" class="form-control" name="last" id="last" value="<?php echo $last;?>"></td><td><p class="error" id="lastError"></p></td></tr>
                                        <tr><td class="addEditLabel">Date of Birth:</td><td><div class='input-group date'>
                                                                <input type='date' name="dob" id="dob" class="form-control" value="<?php echo $dob;?>">
                                                        </div</td><td><p class="error" id="dobError"></p></td></tr>
                                        <tr><td class="addEditLabel">Street Address:</td><td><input type="text" class="form-control" name="street" id="street" value="<?php echo $street?>"></td><td><p class="error" id="streetError"></p></td></tr>
                                        <tr><td class="addEditLabel">State:</td><td><select class="form-control" name="state" id="state">
                                                                <option <?php echo $state == "AL" ? "selected" : "";?> value="AL">Alabama</option>
                                                                <option <?php echo $state == "AK" ? "selected" : "";?> value="AK">Alaska</option>
                                                                <option <?php echo $state == "AZ" ? "selected" : "";?> value="AZ">Arizona</option>
                                                                <option <?php echo $state == "AR" ? "selected" : "";?> value="AR">Arkansas</option>
                                                                <option <?php echo $state == "CA" ? "selected" : "";?> value="CA">California</option>
                                                                <option <?php echo $state == "CO" ? "selected" : "";?> value="CO">Colorado</option>
                                                                <option <?php echo $state == "CT" ? "selected" : "";?> value="CT">Connecticut</option>
                                                                <option <?php echo $state == "DE" ? "selected" : "";?> value="DE">Delaware</option>
                                                                <option <?php echo $state == "DC" ? "selected" : "";?> value="DC">District Of Columbia</option>
                                                                <option <?php echo $state == "FL" ? "selected" : "";?> value="FL">Florida</option>
                                                                <option <?php echo $state == "GA" ? "selected" : "";?> value="GA">Georgia</option>
                                                                <option <?php echo $state == "HI" ? "selected" : "";?> value="HI">Hawaii</option>
                                                                <option <?php echo $state == "ID" ? "selected" : "";?> value="ID">Idaho</option>
                                                                <option <?php echo $state == "IL" ? "selected" : "";?> value="IL">Illinois</option>
                                                                <option <?php echo $state == "IN" ? "selected" : "";?> value="IN">Indiana</option>
                                                                <option <?php echo $state == "IA" ? "selected" : "";?> value="IA">Iowa</option>
                                                                <option <?php echo $state == "KS" ? "selected" : "";?> value="KS">Kansas</option>
                                                                <option <?php echo $state == "KY" ? "selected" : "";?> value="KY">Kentucky</option>
                                                                <option <?php echo $state == "LA" ? "selected" : "";?> value="LA">Louisiana</option>
                                                                <option <?php echo $state == "ME" ? "selected" : "";?> value="ME">Maine</option>
                                                                <option <?php echo $state == "MD" ? "selected" : "";?> value="MD">Maryland</option>
                                                                <option <?php echo $state == "MA" ? "selected" : "";?> value="MA">Massachusetts</option>
                                                                <option <?php echo $state == "MI" ? "selected" : "";?> value="MI">Michigan</option>
                                                                <option <?php echo $state == "MN" ? "selected" : "";?> value="MN">Minnesota</option>
                                                                <option <?php echo $state == "MS" ? "selected" : "";?> value="MS">Mississippi</option>
                                                                <option <?php echo $state == "MO" ? "selected" : "";?> value="MO">Missouri</option>
                                                                <option <?php echo $state == "MT" ? "selected" : "";?> value="MT">Montana</option>
                                                                <option <?php echo $state == "NE" ? "selected" : "";?> value="NE">Nebraska</option>
                                                                <option <?php echo $state == "NV" ? "selected" : "";?> value="NV">Nevada</option>
                                                                <option <?php echo $state == "NH" ? "selected" : "";?> value="NH">New Hampshire</option>
                                                                <option <?php echo $state == "NJ" ? "selected" : "";?> value="NJ">New Jersey</option>
                                                                <option <?php echo $state == "NM" ? "selected" : "";?> value="NM">New Mexico</option>
                                                                <option <?php echo $state == "NY" ? "selected" : "";?> value="NY">New York</option>
                                                                <option <?php echo $state == "NC" ? "selected" : "";?> value="NC">North Carolina</option>
                                                                <option <?php echo $state == "ND" ? "selected" : "";?> value="ND">North Dakota</option>
                                                                <option <?php echo $state == "OH" ? "selected" : "";?> value="OH">Ohio</option>
                                                                <option <?php echo $state == "OK" ? "selected" : "";?> value="OK">Oklahoma</option>
                                                                <option <?php echo $state == "OR" ? "selected" : "";?> value="OR">Oregon</option>
                                                                <option <?php echo $state == "PA" ? "selected" : "";?> value="PA">Pennsylvania</option>
                                                                <option <?php echo $state == "RI" ? "selected" : "";?> value="RI">Rhode Island</option>
                                                                <option <?php echo $state == "SC" ? "selected" : "";?> value="SC">South Carolina</option>
                                                                <option <?php echo $state == "SD" ? "selected" : "";?> value="SD">South Dakota</option>
                                                                <option <?php echo $state == "TN" ? "selected" : "";?> value="TN">Tennessee</option>
                                                                <option <?php echo $state == "TX" ? "selected" : "";?> value="TX">Texas</option>
                                                                <option <?php echo $state == "UT" ? "selected" : "";?> value="UT">Utah</option>
                                                                <option <?php echo $state == "VT" ? "selected" : "";?> value="VT">Vermont</option>
                                                                <option <?php echo $state == "VA" ? "selected" : "";?> value="VA">Virginia</option>
                                                                <option <?php echo $state == "WA" ? "selected" : "";?> value="WA">Washington</option>
                                                                <option <?php echo $state == "WV" ? "selected" : "";?> value="WV">West Virginia</option>
                                                                <option <?php echo $state == "WI" ? "selected" : "";?> value="WI">Wisconsin</option>
                                                                <option <?php echo $state == "WY" ? "selected" : "";?> value="WY">Wyoming</option>
                                                        </select></td><td><p class="error" id="stateError"></p></td></tr>
                                        <tr><td class="addEditLabel">City:</td><td><input type="text" class="form-control" name="city" id="city" value="<?php echo $city;?>"></td><td><p class="error" id="cityError"></p></td></tr>
                                        <tr><td class="addEditLabel">Zip Code:</td><td><input type="text" class="form-control" name="zip" id="zip" value="<?php echo $zip;?>"></td><td><p class="error" id="zipError"></p></td></tr>
                                </table>
                        </div>
                        <input class="btn btn-success" id="btnOpenDialog" type="Submit" value="Submit">
                </form>
        </div>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="js/editValidation.js"></script>
</html>