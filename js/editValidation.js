/**
 * Created by tysonwarner on 4/13/16.
 */
$(document).ready(function() {
    $("#btnOpenDialog").click(function () {
        //debugger;
        var errorCount = 0;
        var form = this.closest('form');
        event.preventDefault();
        var first = $('#first').val();
        var last = $('#last').val();
        var dob = $('#dob').val();
        var street = $('#street').val();
        var state = $('#state').val();
        var city = $('#city').val();
        var zip = $('#zip').val();
        var form = $('#editForm');

        if(/^[a-zA-Z0-9-' ]*$/.test(first) == false) {
            $('#firstError').html("Field contains one or more illegal characters");
            errorCount++;
        } else if ($.trim(first) == "") {
            $('#firstError').html("*Required field");
            errorCount++;
        } else {
            $('#firstError').html("");
        }

        if(/^[a-zA-Z0-9-' ]*$/.test(last) == false) {
            $('#lastError').html("Field contains one or more illegal characters");
            errorCount++;
        } else if ($.trim(last) == "") {
            $('#lastError').html("*Required field");
            errorCount++;
        } else {
            $('#lastError').html("");
        }

        if(/^[0-9-/ ]*$/.test(dob) == false) {
            $('#dobError').html("Field contains one or more illegal characters");
            errorCount++;
        } else if ($.trim(dob) == "") {
            $('#dobError').html("*Required field");
            errorCount++;
        } else {
            $('#dobError').html("");
        }

        if(/^[a-zA-Z0-9-'. ]*$/.test(street) == false) {
            $('#stateError').html("Field contains one or more illegal characters");
            errorCount++;
        } else if ($.trim(street) == "") {
            $('#streetError').html("*Required field");
            errorCount++;
        } else {
            $('#streetError').html("");
        }

        if ($.trim(state) == "") {
            $('#stateError').html("*Required field");
            errorCount++;
        } else {
            $('#stateError').html("");
        }

        if(/^[a-zA-Z-' ]*$/.test(city) == false) {
            $('#cityError').html("Field contains one or more illegal characters");
            errorCount++;
        } else if ($.trim(city) == "") {
            $('#cityError').html("*Required field");
            errorCount++;
        } else {
            $('#cityError').html("");
        }

        if(/^[0-9- ]*$/.test(zip) == false) {
            $('#zipError').html("Field contains one or more illegal characters");
            errorCount++;
        } else if ($.trim(zip) == "") {
            $('#zipError').html("*Required field");
            errorCount++;
        } else {
            $('#zipError').html("");
        }

        if (errorCount == 0) {
            form.submit();
        } else {
            //do nothing
        }
    });
});