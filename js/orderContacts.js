/**
 * Created by tysonwarner on 4/12/16.
 */
function orderContactsByLastName() {
    $.ajax({
        url: "../orderContacts.php",
        type: "GET",
        dataType: 'html',
        data: {
            attribute: 'LASTNAME'
        },
        success: function (response) {
            $('#contacts').html(response);
        },
        error: function (xhr) {
            alert("Error processing request");
        }
    });
    event.preventDefault();
}

$("#filter").change(function (event) {
    var att = $("#filter").val();
    $.ajax({
        url: "../orderContacts.php",
        type: "GET",
        dataType: 'html',
        data: {
            attribute: att
        },
        success: function (response) {
            $('#contacts').html(response);
        },
        error: function (xhr) {
            alert("Error processing request");
        }
    });
    event.preventDefault();
});